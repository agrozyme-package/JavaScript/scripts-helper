"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const child_process_1 = require("child_process");
const [node = '', script = '', ...argumentList] = process.argv;
class Shell {
    static run(command, environment = {}, silent = false) {
        if ('' === command) {
            return false;
        }
        const options = {
            shell: true,
            encoding: 'utf8',
            maxBuffer: Number.MAX_SAFE_INTEGER,
            env: Object.assign({}, process.env, environment)
        };
        if (false === silent) {
            options.stdio = 'inherit';
            console.log(command);
        }
        try {
            const { status, stdout, stderr } = child_process_1.spawnSync(command, [], options);
            if (0 === status) {
                return (null === stdout) ? '' : stdout.trim();
            }
            if (null !== stderr) {
                console.log(stderr);
            }
            return false;
        }
        catch (execption) {
            if (execption instanceof Error) {
                console.log(execption.message);
            }
            return false;
        }
    }
}
// static readonly instance: Shell = new Shell();
Shell.argumentList = argumentList;
Shell.node = node;
Shell.script = script;
exports.default = Shell;
