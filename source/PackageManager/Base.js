"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const fs_1 = require("fs");
const Shell_1 = tslib_1.__importDefault(require("../Shell"));
class Base {
    // protected readonly isWindowsPlatform: boolean = ('win32' === process.platform);
    // noinspection TypeScriptAbstractClassConstructorCanBeMadeProtected
    constructor() {
        this.defaultPackage = {
            name: '',
            path: '',
            link: false
        };
        this.commands = this.getCommands();
        this.globalModulePath = this.getGlobalModulePath();
    }
    addGlobalPackage(item) {
        const command = `${this.commands.addGlobalPackage} ${item}`;
        return ('' !== item) && (false !== Shell_1.default.run(command));
    }
    // noinspection FunctionWithMultipleReturnPointsJS,JSUnusedGlobalSymbols
    getGlobalPackages() {
        const path = this.globalModulePath;
        if ('' === path) {
            return {};
        }
        // noinspection UnusedCatchParameterJS
        try {
            const item = require(path + '/../package.json');
            return item.hasOwnProperty('dependencies') ? item.dependencies : {};
        }
        catch (error) {
            return {};
        }
    }
    linkGlobalPackage(item) {
        const command = `${this.commands.linkGlobalPackage} ${item}`;
        return ('' !== item) && (false !== Shell_1.default.run(command));
    }
    // noinspection FunctionWithMultipleReturnPointsJS
    requireAllGlobalPackages(items) {
        let result = true;
        for (let item of items) {
            if (false === result) {
                return false;
            }
            item = this.preparePackage(item);
            result = this.requireGlobalPackage(item.path, item.name);
            if (result && item.link) {
                result = result && this.linkGlobalPackage(item.name);
            }
        }
        return result;
    }
    requireGlobalPackage(item, module = '') {
        const name = ('' === module) ? item : module;
        return this.existsGlobalPackagePath(name) ? true : this.addGlobalPackage(item);
    }
    existsGlobalPackagePath(name) {
        const path = this.globalModulePath;
        return (('' === path) || ('' === name)) ? false : fs_1.existsSync(path + `/${name}`);
    }
    // noinspection JSMethodCanBeStatic
    getCommands() {
        return {
            getGlobalModulePath: '',
            addGlobalPackage: '',
            linkGlobalPackage: ''
        };
    }
    getGlobalModulePath() {
        let text = Shell_1.default.run(this.commands.getGlobalModulePath, {}, true);
        return (false === text) ? '' : text.toString();
    }
    // noinspection JSMethodCanBeStatic
    preparePackage(item) {
        const source = ('string' === typeof item) ? { name: item } : item;
        const result = Object.assign({}, this.defaultPackage, source);
        if ('' === result.path) {
            result.path = result.name;
        }
        return result;
    }
}
exports.default = Base;
