"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const Base_1 = tslib_1.__importDefault(require("./Base"));
class Empty extends Base_1.default {
    constructor() {
        super();
        console.log(this.getErrorText());
    }
    addGlobalPackage(item) {
        return false;
    }
    getGlobalPackages() {
        return {};
    }
    linkGlobalPackage(item) {
        return false;
    }
    requireAllGlobalPackages(items) {
        return false;
    }
    requireGlobalPackage(item, module = '') {
        return super.requireGlobalPackage(item, module);
    }
    // noinspection JSMethodCanBeStatic
    getErrorText() {
        let text = 'Not support current package manager';
        if (process.stdout.isTTY) {
            const red = '\x1B[31m';
            const normal = '\x1B[0m';
            const error = `${red}error${normal} `;
            text = error + text;
        }
        return text;
    }
}
exports.default = Empty;
