"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const Shell_1 = tslib_1.__importDefault(require("../../Shell"));
const Base_1 = tslib_1.__importDefault(require("../Base"));
class npm extends Base_1.default {
    getCommands() {
        const items = super.getCommands();
        items.addGlobalPackage = 'npm install --global';
        items.getGlobalModulePath = 'npm root --global';
        items.linkGlobalPackage = 'npm link';
        return items;
    }
    getGlobalPackages() {
        const command = 'npm list --global --depth=0 --json';
        let text = Shell_1.default.run(command, {}, true);
        text = (false === text) ? '' : text.toString();
        // noinspection UnusedCatchParameterJS
        try {
            const item = JSON.parse(text);
            return item.hasOwnProperty('dependencies') ? item.dependencies : {};
        }
        catch (error) {
            return {};
        }
    }
}
exports.default = npm;
