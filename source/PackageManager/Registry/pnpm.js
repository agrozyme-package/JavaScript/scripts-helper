"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const Base_1 = tslib_1.__importDefault(require("../Base"));
class pnpm extends Base_1.default {
    getCommands() {
        const items = super.getCommands();
        items.addGlobalPackage = 'pnpm install --global';
        items.getGlobalModulePath = 'pnpm root --global';
        items.linkGlobalPackage = 'pnpm link';
        return items;
    }
}
exports.default = pnpm;
