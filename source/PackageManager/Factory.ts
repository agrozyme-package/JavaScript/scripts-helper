import {parse} from 'path';
import PackageManager, {PackageManagerContructor} from './Base';
import Empty from './Empty';
import npm from './Registry/npm';
import pnpm from './Registry/pnpm';
import yarn from './Registry/yarn';

export class Factory {
  static readonly instance = new Factory();
  readonly managers: { [index: string]: PackageManagerContructor } = {
    npm,
    yarn,
    pnpm
  };
  protected item?: PackageManager;

  protected constructor() {
  }

  detect(): PackageManager {
    if (undefined === this.item) {
      this.item = this.getManager(this.parseMangerName());
    }

    return this.item;
  }

  getManager(name: string): PackageManager {
    const managers = this.managers;

    if (false === managers.hasOwnProperty(name)) {
      return new Empty();
    }

    const classContructor = managers[name];
    return new classContructor();
  }

  // noinspection JSMethodCanBeStatic
  protected parseMangerName(): string {
    const index = 'npm_execpath';

    if (false === process.env.hasOwnProperty(index)) {
      return '';
    }

    const environment = <any>process.env;
    const [name = ''] = parse(environment[index]).name.split('-');
    return name;
  }

}

export default Factory.instance;
