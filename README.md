# scripts-helper

This is a package manager scripts helper.

It has those feature:
- It can add global package and use current package manager to do it.
- It can execute shell command.
- It can inspect object.

Support package managers:
- npm
- yarn

Not support yet:
- pnpm (because I can't make a distinction between npm and pnpm in run-script)

# Note

This module don't support *preinstall* hook.
- Because before install this module, you can't require / import this module.

You can use typescript to write the script code, but it is *not recommand*.
- If you execute ```tsc``` in script, it will complie *.ts to *.js and maybe cause some errors.

Compare with npx:
- npx not support global package execute binary
- if you don't install local package, npx will install each running
- use this module to save disk space and speed running or use npx to get no side effects

# Install

- npm: ``` npm install scripts-helper ```
- yarn: ``` yarn add scripts-helper ```

# Example

If you create a example script, run like:
- npm: ``` npm run example ```
- yarn: ``` yarn run example ```

package.json

```json
{
  "scripts": {
    "build": "node ./.scripts/example.js"
  }
}
```

example.js

```javascript

'use strict';

const script = () => {
  const {PackageManager, Shell, ObjectInspector} = require('@agrozyme/scripts-helper');
  const manager = PackageManager.detect();
  const packages = ['webpack', 'webpack-cli', {name: 'ts-loader', link: true}];

  if (false === manager.requireGlobalPackage('typescript')) {
    process.exit(1);
  }

  if (false === manager.requireAllGlobalPackages(packages)) {
    process.exit(1);
  }

  if (false === Shell.run('tsc')) {
    process.exit(1);
  }

  if (false === Shell.run('webpack')) {
    process.exit(1);
  }

  ObjectInspector.dir(packages, {colors: true});

};

script();

```

